<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function immotool_importer_admin_form($form, &$form_state){
  $form['ftp_url'] = array(
    '#title' => t('FTP-URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ftp_url',''),
    '#required' => TRUE,
    '#description' => t("Write the path relative to your root folder, starting without a '/' at start"),
  /*  '#attached' => array(
      'css' => array(
            'seven' => drupal_get_path('module', 'immoclient') . '/css/immoclient_seven.css',
          ),
      ),*/
  );
  return system_settings_form($form);
}